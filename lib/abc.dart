import 'dart:async';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp_share2/whatsapp_share2.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

// ignore: must_be_immutable
class Abc extends StatefulWidget {

  @override
  State<Abc> createState() => _AbcState();
}

class _AbcState extends State<Abc> {
  File? _image;
  String _phone = '918160994731';

  Future<void> share() async {
    await WhatsappShare.share(
      text: 'Example share text',
      linkUrl: 'https://flutter.dev/',
      phone: _phone,
    );
  }
  Future getImage() async {
    try {
      final ImagePicker _picker = ImagePicker();
      XFile? _pickedFile =
      (await _picker.pickImage(source: ImageSource.gallery));

      if (_pickedFile != null) {
        // getting a directory path for saving
        final directory = await getExternalStorageDirectory();

        // copy the file to a new path
        await _pickedFile.saveTo('${directory!.path}/image1.png');
        _image = File('${directory.path}/image1.png');
      }
    } catch (er) {
      print(er);
    }
  }
  Future<void> shareFile() async {
    await getImage();
    Directory? directory = await getExternalStorageDirectory();

    print('${directory!.path} / ${_image!.path}');
    await WhatsappShare.shareFile(
      phone: _phone,
      filePath: ["${_image!.path}"],
    );
  }

  // Future<void> shareFile() async {
  //   final filePaths = await getPath();
  //     var list = filePaths;
  //     var withoutNulls = list.whereType<String>().toList();
  //   await WhatsappShare.shareFile(
  //     text: 'Whatsapp share text',
  //     phone: _phone,
  //     filePath: ["${withoutNulls}"],
  //   );
  // }

  Future<void> isInstalled() async {
    final val = await WhatsappShare.isInstalled();
    print('Whatsapp is installed: $val');
  }

  // Future<List<String>>pickFile(){}
  @override
  Widget build(BuildContext context) {
    return  SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Whatsapp Share'),
        ),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ElevatedButton(
                child: Text('Share text and link'),
                onPressed: share,
              ),
              ElevatedButton(
                child: Text('Share Image'),
                onPressed: shareFile,
              ),
              ElevatedButton(
                child: Text('is Installed'),
                onPressed: isInstalled,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<List<String?>> getPath() async {
    final result = await FilePicker.platform.pickFiles(allowMultiple: true);
    print('object==>${result?.paths}');
    return result == null ? <String>[] : result.paths;
  }
}